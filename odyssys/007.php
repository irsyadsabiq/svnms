<?php
include_once("../config.inc.php");
$connection = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");

$bulk = new MongoDB\Driver\BulkWrite;

$find_str = array("[", "]");
$_POST['s_mac'] = trim(str_replace($find_str, '', $_POST['s_mac']));

$_POST['s_groups'] = trim(str_replace($find_str, "" , $_POST['s_groups']));
$_POST['s_groups'] = explode(", ", $_POST['s_groups']);

$doc = $_POST;
#var_error_log($_POST);
$doc['_id'] = new MongoDB\BSON\ObjectID;

$milliseconds = microtime(true) * 1000;
$doc['timestamp'] = $milliseconds;

$bulk->insert($doc);

$connection->executeBulkWrite('svnms.odyssys_sessions', $bulk);

?>