<?php
include_once("../config.inc.php");
error_reporting(E_ALL & ~E_NOTICE);
date_default_timezone_set("Asia/Kuala_Lumpur");

//$now = date('Y-m-d H:i:s');
//$mnow = (strtotime($now) * 1000);
$pre = date('Y-m-d H:00:00', strtotime("-1 hour"));
$mpre = strtotime($pre) * 1000;
$end = date('Y-m-d H:59:59', strtotime("-1 hour"));
$mend = strtotime($end) * 1000;

//echo $mpre."\n";
//echo $mend."\n";


include_once("../config.inc.php");

$objects = database_get_object($DB, 'odyssys_tenant', NULL, NULL, NULL);
$connection = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");
$last_updated = time() * 1000;


$temp_store = array();


//aggregation

if($objects){
        if($objects->num_rows > 0){
                while($tenant = $objects->fetch_assoc()){
                        if($tenant['tenant']){

                                $temp_store[$tenant['tenant']] = array();
                                $tenant2 = ['t_tenant' => $tenant['tenant']];
                                $pretime = ['timestamp' =>  ['$gte' => $mpre]];
                                $endtime = ['timestamp' => ['$lte' => $mend]];
                                $time = ['timestamp' =>  ['$gte' => $mpre, '$lte' => $mend]];

                                // start MongoDB - Count Sessions
                                $filter = ['$and' => [$tenant2, $pretime, $endtime]];
                                $query = new MongoDB\Driver\Query($filter);
                                $rows = $connection->executeQuery('svnms.odyssys_sessions', $query);
                                //end MongoDB;
                                //$bulk = new MongoDB\Driver\BulkWrite([]);
                                $data = $rows->toArray();
                                $count_2 = count($data);
                                $temp_store[$tenant['tenant']]['session_count'] = ($count_2 > 0) ? $count_2:0;
                                $temp_store[$tenant['tenant']]['subscriber_count'] = 0;
                                $temp_store[$tenant['tenant']]['ap_count'] = 0;
                                $temp_store[$tenant['tenant']]['start'] = $mpre;
                                $temp_store[$tenant['tenant']]['close'] = $mend;
                                $temp_store[$tenant['tenant']]['timestamp'] = $last_updated;
                        }
                }
        }
}
$command = new MongoDB\Driver\Command([
    'aggregate' => 'odyssys_sessions',
    'pipeline' => [
        ['$match' => ['timestamp' =>  ['$gte' => $mpre, '$lte' => $mend] ] ],
        ['$group' => ['_id'=> '$t_tenant', 'sub_count' => ['$addToSet' => '$s_mac'], 'ap_count' => ['$addToSet' => '$ap_mac'] ] ],
        ['$project'=> ['t_tenant'=> 1,'subscriber'=> ['$size' =>'$sub_count'], 'accesspoint'=> ['$size' =>'$ap_count']]]
        ],
    'cursor' => new stdClass,
]);

$result = $connection->executeCommand('svnms', $command);
$data = $result->toArray();


$bulk = new MongoDB\Driver\BulkWrite([]);
foreach($temp_store as $k => $v){
    foreach($data as $k2 => $v2){
        
        if($k == $data[$k2]->_id){
            $temp_store[$k]['subscriber_count'] = $data[$k2]->subscriber;
            $temp_store[$k]['ap_count'] = $data[$k2]->accesspoint;
        }
        $bulk->update(['t_tenant' => $k, 'start' =>$temp_store[$k]['start'], 'close' =>$temp_store[$k]['close'] ], ['$set' => $temp_store[$k]], ['multi' => false, 'upsert' => true]);
    }
}
$connection->executeBulkWrite("svnms.odyssys_analytics", $bulk);

?>
