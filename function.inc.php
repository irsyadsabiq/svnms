<?php

function database_connect($DBINFO) {
	$conn = new mysqli($DBINFO['HOST'], $DBINFO['USER'], $DBINFO['PASS'], $DBINFO['NAME']) or die("Unable to connect");
	//var_dump($conn);
	return $conn;
}

function database_count_object($DBINFO, $TBLNAME, $WHERE = '', $ORDER = null, $LIMIT = null) {
	$mysqli  = new mysqli($DBINFO['HOST'], $DBINFO['USER'], $DBINFO['PASS'], $DBINFO['NAME']);
	$where   = ($WHERE == '') ? '' : 'WHERE ' . $WHERE;
	$orderby = (!isset($ORDER)) ? '' : 'ORDER BY ' . $ORDER;
	$limit   = (!isset($LIMIT)) ? '' : 'LIMIT ' . $LIMIT;

	$query = "SELECT count(*) as count FROM " . $TBLNAME . " " . $where . " " . $orderby . " " . $limit;
	//var_dump($query);
	$result = $mysqli->query($query);

	if ($result) {
		$count = $result->fetch_assoc();
		return $count['count'];
	}
	return 0;
}

function database_get_object($DBINFO, $TBLNAME, $WHERE = '', $ORDER = null, $LIMIT = null) {
	$mysqli  = new mysqli($DBINFO['HOST'], $DBINFO['USER'], $DBINFO['PASS'], $DBINFO['NAME']);
	$where   = ($WHERE == '') ? '' : 'WHERE ' . $WHERE;
	$orderby = (!isset($ORDER)) ? '' : 'ORDER BY ' . $ORDER;
	$limit   = (!isset($LIMIT)) ? '' : 'LIMIT ' . $LIMIT;

	$query = "SELECT * FROM " . $TBLNAME . " " . $where . " " . $orderby . " " . $limit;
	//var_error_log(trim($query));
	$result = $mysqli->query($query);
	
	//var_dump($result);
	if ($result) {
		return $result;
	}
	return $result;
}

function database_insert_log($DBINFO, $TBLNAME, $LOG) {
	$mysqli = new mysqli($DBINFO['HOST'], $DBINFO['USER'], $DBINFO['PASS'], $DBINFO['NAME']);
	$query  = "INSERT into " . $TBLNAME . " (sid, mac, curtime, code, mesg) values (" . $LOG["sid"] . ", '" . $LOG["mac"] . "', " . $LOG["curtime"] . ", " . $LOG["code"] . ", '" . $LOG["mesg"] . "')";
	// var_dump($query);
	$result = $mysqli->query($query);
}

function database_update($DBINFO, $TBLNAME, $ARR, $ID_TO_UPDATE) {
	$mysqli = new mysqli($DBINFO['HOST'], $DBINFO['USER'], $DBINFO['PASS'], $DBINFO['NAME']);

	$WHERE_STATEMENT = (isset($ID_TO_UPDATE) && trim($ID_TO_UPDATE) != "") ? "WHERE ".$ID_TO_UPDATE : "";  

	$query  = "UPDATE " . $TBLNAME . " SET " .array_to_string($ARR) ." ".$WHERE_STATEMENT;
	///var_dump($query);
	$result = $mysqli->query($query);
}

function array_to_string($arr){
        $str = "";
        if($arr){
		foreach ($arr as $arrk => $arrv){
                	$str .= $arrk ."='".$arrv."'";
                	$str .= next($arr) != null ? ", " : "";
        	}
	}
	
	return $str;
}

function no_header_curl($url){
	
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, true);    // we want headers
	curl_setopt($ch, CURLOPT_NOBODY, true);    // we don't need body
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_TIMEOUT,10);
	$output = curl_exec($ch);
	//var_error_log($output);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	return  $httpcode;
}

function radius_auth($target, $port, $secret_key, $username, $password){

	$radius = radius_auth_open();

	radius_add_server($radius, $target, $port, $secret_key, 5, 3);
	radius_create_request($radius, RADIUS_ACCESS_REQUEST);
	radius_put_attr($radius, RADIUS_USER_NAME, $username);
	radius_put_attr($radius, RADIUS_USER_PASSWORD, $password);
	$result = radius_send_request($radius);
	var_dump($port . ' - ' .$result);
	radius_close($radius);
	if($result){
		switch ($result) {
			case RADIUS_ACCESS_ACCEPT:
  				return 1;
  			break;
			case RADIUS_ACCESS_REJECT:
  				return 0;
  			break;
			case RADIUS_ACCESS_CHALLENGE:
  				return 1;
  			break;
			default:
  				return 0;
		}
	}else{
		return 0;
	}
}

function var_error_log($object = null) {
	ob_start(); // start buffer capture
	var_dump($object); // dump the values
	$contents = ob_get_contents(); // put the buffer into a variable
	ob_end_clean(); // end capture
	error_log($contents); // log contents of the result of var_dump( $object )
}

#List of Api
function api_list($service){

	$sql = "SELECT * FROM svnms.".$service."_api_info";

	$conn = database_connect($GLOBALS['DB']);
	$result = $conn->query($sql);


	while($row = $result->fetch_assoc()) {
		$api[$row["name"]]= $row["url"];
    }

	$conn->close();

	return $api;
}

#Update api
function api_update($api,$name,$status){

	$date = date('Y-m-d H:i:s');

	$sql = "UPDATE svnms.".$api."_api_info SET `status`='".$status."', `updated_at`='".$date."' WHERE `name`='".$name."'";

	$conn = database_connect($GLOBALS['DB']);

	if ($conn->query($sql) === FALSE) {
	    echo "Error updating status: " . $conn->error."\n";
	}

	$conn->close();
}
?>
