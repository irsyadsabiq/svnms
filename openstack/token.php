<?php

include_once(dirname(__DIR__) . '/config.inc.php');
date_default_timezone_set("Asia/Kuala_Lumpur");
$date = date('Y-m-d H:i:s');

#request openstack token api
function requestToken(){
	
	$data = '{\"auth\":{\"identity\":{\"methods\":[\"password\"],\"password\":{\"user\":{\"name\":\"admin\",\"domain\":{\"id\":\"default\"},\"password\":\"nS9ckuPreHJY54I8IvXgUSptimS6CKLfunRaMrpG\"}}}}}';

	$request = "curl -ik \
	--url \"http://172.21.69.22:35357/v3/auth/tokens\" \
	--header \"Content-Type:application/json\" \
	-d '".$data."' ; echo ";

	$cmd = "ssh r1 \"$request\"";
	$output = shell_exec($cmd);

	// Filter output and get token
	$pattern = '/^.*\bX-Subject-Token\b.*$/m';
	preg_match($pattern, $output, $matches);
	$value=explode(" ",$matches[0]);
	$token = $value[1];
	return $token;
	
}

#update openstack token api
function updateToken(){

	//set variables
	$token = requestToken();

	$sql = "UPDATE svnms.tokens SET `token`='".$token."', `updated_at`='".$GLOBALS['date']."' WHERE `name`='openstack'";
	// $sql = "INSERT INTO svnms.tokens (`name`,`token`,`last_updated`) VALUES ('openstack','$token','$time')";

	$conn = database_connect($GLOBALS['DB']);

	if ($conn->query($sql) === FALSE) {
	    echo "Error updating status: " . $conn->error."\n";
	} else {
		echo "\nNew token updated\nNew Token: $token\n\n";
	}

	$conn->close();
}

#get token from mysql db
function getTokenFromDB(){

	$conn = database_connect($GLOBALS['DB']);

	$sql = "SELECT * FROM svnms.tokens WHERE `name`='openstack'";
	$result = $conn->query($sql)->fetch_assoc();

	$token = trim($result['token']);

	$conn->close();
	
	return $token;
}

