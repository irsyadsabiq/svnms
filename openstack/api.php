<?php

include 'token.php';

array_shift($argv);

foreach ($argv as $value) {
	$getApiList = api_list('openstack');
	if (isset($getApiList[$value])) {
		$data = requestOpenstackApi($value,'GET',$getApiList[$value]);
		// echo "$data\n";
		$insertDataMongo = updateMongo($value,$data);
		$insertDataMaria = updateMaria($value,$data);
	} elseif ($argv[0] == 'list') {
		echo "List of api:\n";
		print_r(array_keys($getApiList));
	} else {
		echo "No argument of '$value'\n";
	}
}



#Check token validity
function tokenValidity($code){

	if ($code == 401 ) {
		$updateNewToken = updateToken();
		$tokenValidity = 'invalid';
	} else {
		$tokenValidity = 'valid';
	}
	return $tokenValidity;

}

#Request Api
function requestOpenstackApi($name,$method,$url){

	do {
		$token = getTokenFromDB();

		$request = "curl -s $method \
		--url \"$url\" \
		--header \"X-Auth-Token:$token\"";

		$cmd = "ssh -t r1 \"$request\"";

		$output = shell_exec("$cmd");
		$outputArray = json_decode($output,true);

		if (isset($outputArray['error']['code'])) {
			$code = $outputArray['error']['code'];
		}else {
			$code = 200;
		}

		$updateStatus = api_update('openstack',$name,$code);

		$checkTokenValidity = tokenValidity($code);

	} while ($checkTokenValidity == 'invalid');
	
	return $output;

}

#Update services into mongo
function updateMongo($name,$data){

	$dataArray = json_decode($data,true);
	$dataArray['api_name']= $name;

	try {

		$conn = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");
	    # Insert data
	    // $bulk = new MongoDB\Driver\BulkWrite;
	    // $bulk->delete([]);
	    // $bulk->insert($dataArray);

	    #Update data
	    $bulk = new MongoDB\Driver\BulkWrite([]); 
	    $bulk->update(
		    ['api_name' => $name],
		    ['$set' => $dataArray],
		    ['multi' => false, 'upsert' => false]
		);
	    
	    $result = $conn->executeBulkWrite("svnms.openstack_health", $bulk);
	    echo "Successfully update data '$name' into Mongo\n";

	} catch (MongoDB\Driver\Exception\Exception $e) {

	    $filename = basename(__FILE__);
	    
	    echo "The $filename script has experienced an error.\n"; 
	    echo "It failed with the following exception:\n";
	    
	    echo "Exception:", $e->getMessage(), "\n";
	    echo "In file:", $e->getFile(), "\n";
	    echo "On line:", $e->getLine(), "\n";       
	}
}

#Update services into maria
function updateMaria($name,$data){

	if ($name == 'compute_services') {
		$updateMaria = computeService($name,$data);
	}else if ($name == 'hypervisors_detail') {
		$updateMaria = hypervisorsDetail($name,$data);
	}else if ($name == 'network_agents') {
		$updateMaria = networkAgents($name,$data);
	}else if ($name == 'block_storage_services') {
		$updateMaria = blockStorageService($name,$data);
	}
}

function computeService($name,$data){

	$conn = database_connect($GLOBALS['DB']);
	$date = date('Y-m-d H:i:s');

	$dataArray = json_decode($data,true);
	$columnServices = "`id`,`host`,`zone`,`binary`,`status`,`state`,`updated_at`";

	$status = 'success';

	foreach ($dataArray['services'] as $service) {
		$val['id'] = $service['id'];
		$val['host'] = $service['host'];
		$val['zone'] = $service['zone'];
		$val['binary'] = $service['binary'];
		$val['status'] = $service['status'];
		$val['state'] = $service['state'];
		$val['updated_at'] = $date;
		$sqlValue = "'".implode("', '",$val)."'";
        $sql = "REPLACE INTO openstack_$name ($columnServices) VALUES ($sqlValue)";

        if ($conn->query($sql) === FALSE) {
            echo "Error creating/updating record: " . $conn->error."\n ID: ".$val['id']."\n";
            $status = 'failed';
        }
	}

	if ($status == 'success') {
		echo "Successfully update data '$name' into Maria\n";
	}else {
		echo "Failed update data '$name' into Mongo\n";
	}
	
}

function hypervisorsDetail($name,$data){

	$conn = database_connect($GLOBALS['DB']);
	$date = date('Y-m-d H:i:s');

	$dataArray = json_decode($data,true);
	$columnServices = "`id`,`host`,`service_id`,`host_ip`,`vcpus`,`vcpus_used`,`status`,`state`,`updated_at`";
	$status = 'success';

	foreach ($dataArray['hypervisors'] as $service) {
		$val['id'] = $service['id'];
		$val['host'] = $service['service']['host'];
		$val['service_id'] = $service['service']['id'];
		$val['host_ip'] = $service['host_ip'];
		$val['vcpus'] = $service['vcpus'];
		$val['vcpus_used'] = $service['vcpus_used'];
		$val['status'] = $service['status'];
		$val['state'] = $service['state'];
		$val['updated_at'] = $date;
		$sqlValue = "'".implode("', '",$val)."'";
        $sql = "REPLACE INTO openstack_$name ($columnServices) VALUES ($sqlValue)";

        // print_r($sql);

        if ($conn->query($sql) === FALSE) {
            echo "Error creating/updating record: " . $conn->error."\n ID: ".$val['id']."\n";
            $status = 'failed';
        }
	}

	if ($status == 'success') {
		echo "Successfully update data '$name' into Maria\n";
	}else {
		echo "Failed update data '$name' into Mongo\n";
	}
}

function networkAgents($name,$data){

	$conn = database_connect($GLOBALS['DB']);
	$date = date('Y-m-d H:i:s');

	$dataArray = json_decode($data,true);
	$columnServices = "`id`,`host`,`binary`,`agent_type`,`alive`,`updated_at`";
	$status = 'success';

	foreach ($dataArray['agents'] as $service) {
		$val['id'] = $service['id'];
		$val['host'] = $service['host'];
		$val['binary'] = $service['binary'];
		$val['agent_type'] = $service['agent_type'];
		$val['alive'] = $service['alive'];
		$val['updated_at'] = $date;
		$sqlValue = "'".implode("', '",$val)."'";
        $sql = "REPLACE INTO openstack_$name ($columnServices) VALUES ($sqlValue)";

        if ($conn->query($sql) === FALSE) {
            echo "Error creating/updating record: " . $conn->error."\n ID: ".$val['id']."\n";
            $status = 'failed';
        }
	}

	if ($status == 'success') {
		echo "Successfully update data '$name' into Maria\n";
	}else {
		echo "Failed update data '$name' into Mongo\n";
	}
	
}

function blockStorageService($name,$data){

	$conn = database_connect($GLOBALS['DB']);
	$date = date('Y-m-d H:i:s');

	$dataArray = json_decode($data,true);
	$columnServices = "`host`,`binary`,`status`,`state`,`updated_at`";
	$status = 'success';

	foreach ($dataArray['services'] as $service) {
		$val['host'] = $service['host'];
		$val['binary'] = $service['binary'];
		$val['status'] = $service['status'];
		$val['state'] = $service['state'];
		$val['updated_at'] = $date;
		$sqlValue = "'".implode("', '",$val)."'";
        $sql = "REPLACE INTO openstack_$name ($columnServices) VALUES ($sqlValue)";

        if ($conn->query($sql) === FALSE) {
            echo "Error creating/updating record: " . $conn->error."\n Host: ".$val['host']."\n";
            $status = 'failed';
        }
	}

	if ($status == 'success') {
		echo "Successfully update data '$name' into Maria\n";
	}else {
		echo "Failed update data '$name' into Mongo\n";
	}
	
}




