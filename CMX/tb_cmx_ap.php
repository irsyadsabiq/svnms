<?php
## Indexing: db.cmx_ap.ensureIndex({"id": 1})

#set variables
$time=time();
date_default_timezone_set("Asia/Kuala_Lumpur");
$last_updated = date("Y/m/d H:i",$time);
$url = 'https://duneadmin:Dune1234@172.21.68.12/api/config/v1/aps';
$first = 0;
$max = 1000;
$bulk = new MongoDB\Driver\BulkWrite([]);
$mon_conn = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");

    $time = time();
    # curl the API
    // $listDevicesUrl= "$url?.full=true&.firstResult=$first&.maxResults=$max";
    $listDevices = exec("curl -s -k '$url'");

    #get return to array
    $listDevicesArray = json_decode($listDevices,true);
    foreach($listDevicesArray as $listDevicesArray){
    // echo $listDevicesArray['name']."\n";
    // echo $listDevicesArray['radioMacAddress']."\n";
    // echo $listDevicesArray['mapCoordinates']['x']."\n";
    // echo $listDevicesArray['mapCoordinates']['y']."\n";

    try {
      //MACAddress as ID
      $id = $listDevicesArray['radioMacAddress'];

      // MongoDB UPSERT dump
      $bulk = new MongoDB\Driver\BulkWrite([]);
      $bulk->update(
              ['id' => $id],
              ['$set'=>$listDevicesArray],
              ['upsert' => true]
      );
      $bulk->update(
              ['id' => $id],
              ['$set'=>['last_updated'=>$time]],
              ['upsert' => true]
      );
      $mon_conn->executeBulkWrite("svnms.cmx_ap", $bulk);


      ## UPSERT into cleanup MongoDB
      $bulk = new MongoDB\Driver\BulkWrite([]);
      $data['Name']=$listDevicesArray['name'];
      $data['MacAddress']=$listDevicesArray['radioMacAddress'];
      $data['X']=$listDevicesArray['mapCoordinates']['x'];
      $data['Y']=$listDevicesArray['mapCoordinates']['y'];
      $data['last_updated']=$last_updated;
      $bulk->update(
              ['id' => $id],
              ['$set'=>$data],
              ['upsert' => true]
      );
      $mon_conn->executeBulkWrite("svnms.tb_cmx_ap", $bulk);

  }catch (MongoDB\Driver\Exception\Exception $e) {

      $filename = basename(__FILE__);

      echo "The $filename script has experienced an error.\n";
      echo "It failed with the following exception:\n";
  }
}

?>
