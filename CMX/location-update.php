<?php
date_default_timezone_set("Asia/Kuala_Lumpur");


function var_error_log( $object=null ){
    ob_start();                    // start buffer capture
    var_dump( $object );           // dump the values
    $contents = ob_get_contents(); // put the buffer into a variable
    ob_end_clean();                // end capture
    error_log( $contents );        // log contents of the result of var_dump( $object )
}
$data = file_get_contents("php://input");
var_error_log( $data );

$collectionName = "location_update_notifications";

upsertMongo($collectionName,$data);

#Upsert notifications into mongo
function upsertMongo($collectionName,$data){

    try {

        $conn = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");
            # Insert data

            $arrayData = json_decode($data,true);
            $id = $arrayData['notifications'][0]['eventId'];

            $bulk = new MongoDB\Driver\BulkWrite([]);
            $bulk->update(
                    ['id' => $id],
                    ['$set' => $arrayData],
                    ['upsert' => true]
            );
        $result = $conn->executeBulkWrite("svnms.cmx_".$collectionName, $bulk);
        //var_error_log($bulk); 
    } catch (MongoDB\Driver\Exception\Exception $e) {

        $filename = basename(__FILE__);
//var_error_log($e);
        echo "The $filename script has experienced an error.\n";
        echo "It failed with the following exception:\n";

        echo "Exception:", $e->getMessage(), "\n";
        echo "In file:", $e->getFile(), "\n";
        echo "On line:", $e->getLine(), "\n";
    }
}
