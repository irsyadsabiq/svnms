<?php

## Indexing: db.cmx_client.ensureIndex({"id": 1})
#set variables
$url = 'https://duneadmin:Dune1234@172.21.68.12/api/location/v2/clients';
// $count=0;
$max = 1000;
$bulk = new MongoDB\Driver\BulkWrite([]);
$mon_conn = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");


    $time = time();
    # curl the API
    // $listDevicesUrl= "$url?.full=true&.firstResult=$first&.maxResults=$max";
    $listDevices = exec("curl -s -k '$url'");

    #get return to array
    $listDevicesArray = json_decode($listDevices,true);
    echo count($listDevicesArray);
    foreach($listDevicesArray as $listDevicesArray){
    // echo $listDevicesArray['macAddress']."\n";
    // echo $listDevicesArray['dot11Status']."\n";
    // echo $listDevicesArray['mapCoordinate']['x']."\n";
    // echo $listDevicesArray['mapCoordinate']['y']."\n";
    // echo $listDevicesArray['apMacAddress']."\n";
    // echo $listDevicesArray['statistics']['maxDetectedRssi']['apMacAddress']."\n";
    // echo $listDevicesArray['statistics']['firstLocatedTime']."\n";
    // echo $listDevicesArray['statistics']['lastLocatedTime']."\n";
    // echo $listDevicesArray['mapInfo']['mapHierarchyString']."\n";
    // echo $time;
    try {
      //MACAddress as ID
      $id = $listDevicesArray['macAddress'];

      // MongoDB UPSERT dump
      $bulk = new MongoDB\Driver\BulkWrite([]);
      $bulk->update(
              ['id' => $id],
              ['$set'=>$listDevicesArray],
              ['upsert' => true]
      );
      $bulk->update(
              ['id' => $id],
              ['$set'=>['last_updated'=>$time]],
              ['upsert' => true]
      );
      $mon_conn->executeBulkWrite("svnms.cmx_client", $bulk);

      ## UPSERT into cleanup MongoDB
      $bulk = new MongoDB\Driver\BulkWrite([]);
      $data['ClientMacAddress']=$listDevicesArray['macAddress'];
      $data['Dot11Status']=$listDevicesArray['dot11Status'];
      $data['X']=$listDevicesArray['mapCoordinate']['x'];
      $data['Y']=$listDevicesArray['mapCoordinate']['y'];
      $data['ConnectedAPMacAddress']=$listDevicesArray['apMacAddress'];
      $data['DetectedAPMacAddress']=$listDevicesArray['statistics']['maxDetectedRssi']['apMacAddress'];
      $data['FirstLocatedTime']=$listDevicesArray['statistics']['firstLocatedTime'];
      $data['LastLocatedTime']=$listDevicesArray['statistics']['lastLocatedTime'];
      $data['MapHierarchyString']=$listDevicesArray['mapInfo']['mapHierarchyString'];
      $data['last_updated']=$time;

      $bulk->update(
              ['id' => $id],
              ['$set'=>$data],
              ['upsert' => true]
      );
      $mon_conn->executeBulkWrite("svnms.cmx_client_info", $bulk);

  }catch (MongoDB\Driver\Exception\Exception $e) {

      $filename = basename(__FILE__);

      echo "The $filename script has experienced an error.\n";
      echo "It failed with the following exception:\n";
  }
}


?>
