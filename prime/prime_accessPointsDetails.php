<?php
# MongoDB indexing : db.prime_access_points_details.ensureIndex({"id": 1})
include_once("../config.inc.php");
$mar_conn = new mysqli($DB['HOST'], $DB['USER'], $DB['PASS'], $DB['NAME']);
#set variables
$milliseconds = round(microtime(true) * 1000);
$last_updated = $milliseconds;
$url = 'https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v3/data/AccessPointDetails.json';

$tablePrime = '`svnms`.`prime`';

$first = 0;
$max = 1000;

$mon_conn = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");

#Infinity curl
while ($first>-1) {
    $date1 = time();
    $bulk = new MongoDB\Driver\BulkWrite([]);
    # curl the API
    $listAlarmsUrl= "$url?.full=true&.firstResult=$first&.maxResults=$max";
    $listAlarms = exec("curl -s -k '$listAlarmsUrl'");

    #get return to array
    $listAlarmsArray = json_decode($listAlarms,true);
    $entities = $listAlarmsArray['queryResponse']['entity'];
    $statusChecking = $listAlarmsArray['queryResponse']['@count'];

    #break if no output
    if (!$entities) break;

    foreach ($entities as $entity) {
      $cdpNeighbors = $entity['accessPointDetailsDTO']['cdpNeighbors']['cdpNeighbor'];
      $wlanProfiles = $entity['accessPointDetailsDTO']['unifiedApInfo']['wlanProfiles']['wlanProfile'];

        //Prepare variable's values
        $document = $entity_aux = array();
        $entity_aux['@id'] = $entity['accessPointDetailsDTO']['@id'];
        $entity_aux['adminStatus'] = $entity['accessPointDetailsDTO']['adminStatus'];
        $entity_aux['apType'] = $entity['accessPointDetailsDTO']['apType'];
        ## CDP neighbor
        foreach ( $cdpNeighbors as $cdpNeighbor)
        {
        $entity_aux['cdpNeighbor_capabilities'] = $cdpNeighbor['capabilities'];
        $entity_aux['cdpNeighbor_duplex'] = $cdpNeighbor['duplex'];
        $entity_aux['cdpNeighbor_interfaceSpeed'] = $cdpNeighbor['interfaceSpeed'];
        $entity_aux['cdpNeighbor_localPort'] = $cdpNeighbor['localPort'];
        $entity_aux['cdpNeighbor_neighborIpAddress'] = $cdpNeighbor['neighborIpAddress'];
        $entity_aux['cdpNeighbor_neighborName'] = $cdpNeighbor['neighborName'];
        $entity_aux['cdpNeighbor_neighborPort'] =$cdpNeighbor['neighborPort'];
        $entity_aux['cdpNeighbor_platform'] = $cdpNeighbor['platform'];
        }
        $entity_aux['clientCount'] = $entity['accessPointDetailsDTO']['clientCount'];
        $entity_aux['clientCount_2_4GHz'] = $entity['accessPointDetailsDTO']['clientCount_2_4GHz'];
        $entity_aux['clientCount_5GHz'] = $entity['accessPointDetailsDTO']['clientCount_5GHz'];
        $entity_aux['ethernetMac'] = $entity['accessPointDetailsDTO']['ethernetMac'];
        $entity_aux['ipAddress'] = $entity['accessPointDetailsDTO']['ipAddress'];
        $entity_aux['locationHierarchy'] = $entity['accessPointDetailsDTO']['locationHierarchy'];
        $entity_aux['macAddress'] = $entity['accessPointDetailsDTO']['macAddress'];
        $entity_aux['mapLocation'] = $entity['accessPointDetailsDTO']['mapLocation'];
        $entity_aux['model'] = $entity['accessPointDetailsDTO']['model'];
        $entity_aux['name'] = $entity['accessPointDetailsDTO']['name'];
        $entity_aux['reachabilityStatus'] = $entity['accessPointDetailsDTO']['reachabilityStatus'];
        $entity_aux['serialNumber'] = $entity['accessPointDetailsDTO']['serialNumber'];
        $entity_aux['softwareVersion'] = $entity['accessPointDetailsDTO']['softwareVersion'];
        $entity_aux['status'] = $entity['accessPointDetailsDTO']['status'];
        $entity_aux['type'] = $entity['accessPointDetailsDTO']['type'];
        $entity_aux['unifiedApInfo_WIPSEnabled'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['WIPSEnabled'];
        $entity_aux['unifiedApInfo_apCertType'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['apCertType'];
        $entity_aux['unifiedApInfo_apGroupName'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['apGroupName'];
        $entity_aux['unifiedApInfo_apMode'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['apMode'];
        $entity_aux['unifiedApInfo_apStaticEnabled'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['apStaticEnabled'];
        $entity_aux['unifiedApInfo_bootVersion'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['bootVersion'];
        $entity_aux['unifiedApInfo_capwapJoinTakenTime'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['capwapJoinTakenTime'];
        $entity_aux['unifiedApInfo_capwapUpTime'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['capwapUpTime'];
        $entity_aux['unifiedApInfo_controllerIpAddress'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['controllerIpAddress'];
        $entity_aux['unifiedApInfo_controllerName'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['controllerName'];
        $entity_aux['unifiedApInfo_contryCode'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['contryCode'];
        $entity_aux['unifiedApInfo_encryptionEnabled'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['encryptionEnabled'];
        $entity_aux['unifiedApInfo_flexConnectMode'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['flexConnectMode'];
        $entity_aux['unifiedApInfo_iosVersion'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['iosVersion'];
        $entity_aux['unifiedApInfo_linkLatencyEnabled'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['linkLatencyEnabled'];
        $entity_aux['unifiedApInfo_poeStatus'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['poeStatus'];
        $entity_aux['unifiedApInfo_poeStatusEnum'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['poeStatusEnum'];
        $entity_aux['unifiedApInfo_portNumber'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['portNumber'];
        $entity_aux['unifiedApInfo_preStandardState'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['preStandardState'];
        $entity_aux['unifiedApInfo_primaryMwar'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['primaryMwar'];
        $entity_aux['unifiedApInfo_rogueDetectionEnabled'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['rogueDetectionEnabled'];
        $entity_aux['unifiedApInfo_secondaryMwar'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['secondaryMwar'];
        $entity_aux['unifiedApInfo_sshEnabled'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['sshEnabled'];
        $entity_aux['unifiedApInfo_statisticsTimer'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['statisticsTimer'];
        $entity_aux['unifiedApInfo_telnetEnabled'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['telnetEnabled'];
        $entity_aux['unifiedApInfo_WIPSEnabled'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['WIPSEnabled'];
        ## WLAN profile
        foreach ($wlanProfiles as $wlanProfile)
        {
        $entity_aux['wlanProfiles_broadcastSsidEnabled'] = $wlanProfile['broadcastSsidEnabled'];
        $entity_aux['wlanProfiles_profileName'] = $wlanProfile['profileName'];
        $entity_aux['wlanProfiles_ssid'] = $wlanProfile['ssid'];
        }
        $entity_aux['unifiedApInfo_wlanVlanMappings'] = $entity['accessPointDetailsDTO']['unifiedApInfo']['wlanVlanMappings'];
        $entity_aux['upTime'] = $entity['accessPointDetailsDTO']['upTime'];
        $entity_aux['last_updated'] = $milliseconds;

        // MongoDB UPSERT
        $document['where'] = array('@id' => $entity_aux['@id']);
        $document['data'] = array('$set' => $entity_aux);
        $document['options'] = array('multi' => false, 'upsert' => true);
        $bulk->update(['@id' => $entity_aux['@id']], ['$set' => $entity_aux], ['multi' => false, 'upsert' => true]);
        $entity_aux['@id']."-".$entity_aux['unifiedApInfo_controllerIpAddress']."\n";

        // MariaDB
        $value['id'] = $entity_aux['@id'];
        $value['ClientCount'] = $entity_aux['clientCount'];
        $value['clientCount_2_4GHz'] = $entity_aux['clientCount_2_4GHz'];
        $value['clientCount_5GHz'] = $entity_aux['clientCount_5GHz'];
        $value['ethernetMac'] = $entity_aux['ethernetMac'];
        $value['ipAddress'] = $entity_aux['ipAddress'];
        $value['name'] = $entity_aux['name'];
        $value['reachabilityStatus'] = $entity_aux['reachabilityStatus'];
        $value['apGroupName'] = $entity_aux['unifiedApInfo_apGroupName'];
        $value['ssid'] = $entity_aux['wlanProfiles_ssid'];
        $value['upTime'] = $entity_aux['upTime'];
        $value['last_updated'] = $entity_aux['last_updated'];

        ## insert into Mysql
        $columns ="id, ClientCount, clientCount_2_4GHz, clientCount_5GHz, ethernetMac, ipAddress, name, reachabilityStatus, apGroupName, ssid, upTime, last_updated";
        $values = "'".implode("', '",$value)."'";
        $sql = "REPLACE INTO prime_access_points_details ($columns) VALUES ($values)";
        // var_dump($sql);
        $mar_conn->query($sql);

    }
    $mon_conn->executeBulkWrite("svnms.prime_access_points_details", $bulk);
    $first+=$max;
    echo $first;
}
?>
