<?php
# MongoDB indexing : db.prime_access_points_details.ensureIndex({"id": 1})
// include_once("../config.inc.php");
// $mar_conn = new mysqli($DB['HOST'], $DB['USER'], $DB['PASS'], $DB['NAME']);
#set variables
$time=time();
date_default_timezone_set("Asia/Kuala_Lumpur");
$last_updated = date("Y/m/d H:i",$time);

$url = 'https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v3/data/AccessPointDetails.json';

$first = 0;
$max = 1000;

$mon_conn = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");

#Infinity curl
while ($first>-1) {
    // $date1 = time();
    $bulk = new MongoDB\Driver\BulkWrite([]);
    # curl the API
    $listAlarmsUrl= "$url?.full=true&.firstResult=$first&.maxResults=$max";
    $listAlarms = exec("curl -s -k '$listAlarmsUrl'");

    #get return to array
    $listAlarmsArray = json_decode($listAlarms,true);
    $entities = $listAlarmsArray['queryResponse']['entity'];
    $statusChecking = $listAlarmsArray['queryResponse']['@count'];

    #break if no output
    if (!$entities) break;

    foreach ($entities as $entity) {
        $wlanProfiles = $entity['accessPointDetailsDTO']['unifiedApInfo']['wlanProfiles']['wlanProfile'];

        //Prepare variable's values
        $document = $entity_aux = array();
        $entity_aux['@id'] = $entity['accessPointDetailsDTO']['@id'];
        $entity_aux['clientCount_2_4GHz'] = $entity['accessPointDetailsDTO']['clientCount_2_4GHz'];
        $entity_aux['clientCount_5GHz'] = $entity['accessPointDetailsDTO']['clientCount_5GHz'];
        $entity_aux['status'] = $entity['accessPointDetailsDTO']['status'];
        foreach ($wlanProfiles as $wlanProfile)
        {
        $entity_aux['ssid'] = $wlanProfile['ssid'];
        }
        $entity_aux['last_updated'] = $last_updated;

        // MongoDB UPSERT
        $document['where'] = array('@id' => $entity_aux['@id']);
        $document['data'] = array('$set' => $entity_aux);
        $document['options'] = array('multi' => false, 'upsert' => true);
        $bulk->update(['@id' => $entity_aux['@id']], ['$set' => $entity_aux], ['multi' => false, 'upsert' => true]);


    }
    $mon_conn->executeBulkWrite("svnms.tb_prime_ap_statistics", $bulk);
    $first+=$max;
    echo $first;
}
?>
