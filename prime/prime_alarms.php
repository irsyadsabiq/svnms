<?php
// include_once("../config.inc.php");
// $mar_conn = new mysqli($DB['HOST'], $DB['USER'], $DB['PASS'], $DB['NAME']);
# MongoDB indexing : db.prime_alarms.ensureIndex({"id": 1})
#set variables
$milliseconds = round(microtime(true) * 1000);
$last_updated = $milliseconds;
$url = 'https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v3/data/Alarms.json';

$tablePrime = '`svnms`.`prime`';

$first = 0;
$max = 1000;

#Infinity curl
while ($first>-1) {
    $date1 = time();
    $bulk = new MongoDB\Driver\BulkWrite([]);
    # curl the API
    $listAlarmsUrl= "$url?.full=true&.firstResult=$first&.maxResults=$max&";
    $listAlarms = exec("curl -s -k '$listAlarmsUrl'");

    #get return to array
    $listAlarmsArray = json_decode($listAlarms,true);
    $entities = $listAlarmsArray['queryResponse']['entity'];
    $statusChecking = $listAlarmsArray['queryResponse']['@count'];

    #break if no output
    if (!$entities) break;

    foreach ($entities as $entity) {
        $entity['id'] = $entity['alarmsDTO']['@id'];
        $a = upsert($entity);

    }
    $first+=$max;
    echo $first;
}

## update mongoDB ##
function upsert($data){
  try {

    $mon_conn = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");
      # Insert data

      $id = $data['id'];

      $bulk = new MongoDB\Driver\BulkWrite([]);
      $bulk->update(
        ['id' => $id],
        ['$set' => $data],
        ['upsert' => true]
    );
      $result = $mon_conn->executeBulkWrite('svnms.prime_alarms', $bulk);


  } catch (MongoDB\Driver\Exception\Exception $e) {

      $filename = basename(__FILE__);

      echo "The $filename script has experienced an error.\n";
      echo "It failed with the following exception:\n";

      echo "Exception:", $e->getMessage(), "\n";
      echo "In file:", $e->getFile(), "\n";
      echo "On line:", $e->getLine(), "\n";
  }
}
?>
