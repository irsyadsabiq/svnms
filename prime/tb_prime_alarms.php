<?php
# MongoDB indexing : db.prime_alarms_filtered.ensureIndex({"id": 1})
include_once("../config.inc.php");
$mar_conn = new mysqli($DB['HOST'], $DB['USER'], $DB['PASS'], $DB['NAME']);
$mon_conn = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");
#set variables
$time=time();
date_default_timezone_set("Asia/Kuala_Lumpur");
$last_updated = date("Y/m/d H:i",$time);

$url = 'https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v3/data/Alarms.json';

$tablePrime = '`svnms`.`prime`';

$first = 0;
$max = 1000;


#Infinity curl
while ($first>-1) {
    $date1 = time();
    $bulk = new MongoDB\Driver\BulkWrite([]);
    # curl the API
    $listAlarmsUrl= "$url?.full=true&.or_filter=true&.firstResult=$first&.maxResults=$max&severity=MAJOR&severity=MINOR&severity=CRITICAL&";
    $listAlarms = exec("curl -s -k '$listAlarmsUrl'");

    #get return to array
    $listAlarmsArray = json_decode($listAlarms,true);
    $entities = $listAlarmsArray['queryResponse']['entity'];
    $statusChecking = $listAlarmsArray['queryResponse']['@count'];

    #break if no output
    if (!$entities) break;

    foreach ($entities as $entity) {
        $entity['id'] = $entity['alarmsDTO']['@id'];

        //Prepare variable's values
        $document = $entity_aux = array();
        $entity_aux['@id'] = $entity['alarmsDTO']['@id'];
        // $entity_aux['acknowledgementStatus'] = $entity['alarmsDTO']['acknowledgementStatus'];
        // $entity_aux['alarmFoundAt'] = $entity['alarmsDTO']['alarmFoundAt'];
        // $entity_aux['alarmID'] = $entity['alarmsDTO']['alarmId'];
        // $entity_aux['category_ordinal'] = $entity['alarmsDTO']['category']['ordinal'];
        $entity_aux['category_value'] = $entity['alarmsDTO']['category']['value'];
        // $entity_aux['condition_ordinal'] = $entity['alarmsDTO']['condition']['ordinal'];
        $entity_aux['condition_value'] = $entity['alarmsDTO']['condition']['value'];
        // $entity_aux['deviceName'] = $entity['alarmsDTO']['deviceName'];
        // $entity_aux['lastUpdatedAt'] = $entity['alarmsDTO']['lastUpdatedAt'];
        $entity_aux['message'] = $entity['alarmsDTO']['message'];
        // $entity_aux['nttyaddrss7_address'] = str_replace(" ","",$entity['alarmsDTO']['nttyaddrss7_address']);
        $entity_aux['severity'] = trim($entity['alarmsDTO']['severity']);
        // $entity_aux['source'] = $entity['alarmsDTO']['source'];
        // $entity_aux['timestamp_prime'] = $entity['alarmsDTO']['timeStamp'];
        // $entity_aux['wirelessSpecificAlarmId'] = $entity['alarmsDTO']['wirelessSpecificAlarmId'];
        $entity_aux['last_updated']=$last_updated;

        // MongoDB UPSERT
        $document['where'] = array('@id' => $entity_aux['@id']);
        $document['data'] = array('$set' => $entity_aux);
        $document['options'] = array('multi' => false, 'upsert' => true);
        $bulk->update(['@id' => $entity_aux['@id']], ['$set' => $entity_aux], ['multi' => false, 'upsert' => true]);


        // // MariaDB UPSERT
        // $value['id'] = $entity_aux['@id'];
        // $value['acknowledgementStatus'] = $entity_aux['acknowledgementStatus'];
        // $value['alarmFoundAt'] = $entity_aux['alarmFoundAt'];
        // $value['alarmID'] = $entity_aux['alarmId'];
        // $value['category_ordinal'] = $entity_aux['category_ordinal'];
        // $value['category_value'] = $entity_aux['category_value'];
        // $value['condition_ordinal'] = $entity_aux['condition_ordinal'];
        // $value['condition_value'] = $entity_aux['condition_value'];
        // $value['deviceName'] = $entity_aux['deviceName'];
        // $value['lastUpdatedAt'] = $entity_aux['lastUpdatedAt'];
        // $value['message'] = $entity_aux['message'];
        // $value['nttyaddrss7_address'] = $entity_aux['nttyaddrss7_address'];
        // $value['severity'] = $entity_aux['severity'];
        // $value['source'] = $entity_aux['source'];
        // $value['timestamp_prime'] = $entity_aux['timestamp_prime'];
        // $value['wirelessSpecificAlarmId'] = $entity_aux['wirelessSpecificAlarmId'];
        // $value['last_updated'] = $entity_aux['last_updated'];
        // $values = "\"".implode('", "',$entity_aux)."\"";
        // //filtering "Severity != CLEARED"
        //  $sql = "INSERT INTO prime_alarm (id, acknowledgementStatus, alarmFoundAt, alarmID, category_ordinal, category_value, condition_ordinal, condition_value, deviceName, lastUpdatedAt, message, nttyaddrss7_address, severity, source, timestamp_prime, wirelessSpecificAlarmId, last_updated) VALUES ($values)";
        //  var_dump($sql);
        //  $mar_conn->query($sql);


    }

    // $mon_conn->executeBulkWrite("svnms.prime_alarms", $bulk);
    $mon_conn->executeBulkWrite("svnms.tb_prime_alarms", $bulk);
    $first+=$max;
    echo $first;
}

?>
