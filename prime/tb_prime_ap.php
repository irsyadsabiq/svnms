<?php
# MongoDB indexing : db.prime_access_points_details.ensureIndex({"id": 1})
// include_once("../config.inc.php");
// $mar_conn = new mysqli($DB['HOST'], $DB['USER'], $DB['PASS'], $DB['NAME']);
#set variables
$time=time();
date_default_timezone_set("Asia/Kuala_Lumpur");
$last_updated = date("Y/m/d H:i",$time);

$url = 'https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v3/data/AccessPointDetails.json';

$first = 0;
$max = 1000;

$mon_conn = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");

#Infinity curl
while ($first>-1) {
    // $date1 = time();
    $bulk = new MongoDB\Driver\BulkWrite([]);
    # curl the API
    $listAlarmsUrl= "$url?.full=true&.firstResult=$first&.maxResults=$max";
    $listAlarms = exec("curl -s -k '$listAlarmsUrl'");

    #get return to array
    $listAlarmsArray = json_decode($listAlarms,true);
    $entities = $listAlarmsArray['queryResponse']['entity'];
    $statusChecking = $listAlarmsArray['queryResponse']['@count'];

    #break if no output
    if (!$entities) break;

    foreach ($entities as $entity) {


        //Prepare variable's values
        $document = $entity_aux = array();
        $entity_aux['@id'] = $entity['accessPointDetailsDTO']['@id'];
        $entity_aux['name'] = $entity['accessPointDetailsDTO']['name'];
        $entity_aux['reachabilityStatus'] = $entity['accessPointDetailsDTO']['reachabilityStatus'];
        ## change reachability value to YES or NO
        if($entity_aux['reachabilityStatus'] == 'REACHABLE') {
          $entity_aux['reachabilityStatus']='YES';
        }else{
          $entity_aux['reachabilityStatus']='NO';
        }

        $entity_aux['ipAddress'] = $entity['accessPointDetailsDTO']['ipAddress'];
        $entity_aux['locationHierarchy'] = $entity['accessPointDetailsDTO']['locationHierarchy'];
        $entity_aux['macAddress'] = $entity['accessPointDetailsDTO']['macAddress'];
        $entity_aux['upTime'] = $entity['accessPointDetailsDTO']['upTime'];
        $entity_aux['last_updated'] = $last_updated;

        // MongoDB UPSERT
        $document['where'] = array('@id' => $entity_aux['@id']);
        $document['data'] = array('$set' => $entity_aux);
        $document['options'] = array('multi' => false, 'upsert' => true);
        $bulk->update(['@id' => $entity_aux['@id']], ['$set' => $entity_aux], ['multi' => false, 'upsert' => true]);

        // MariaDB UPSERT
        // $value['id'] = $entity_aux['@id'];
        // $value['name'] = $entity_aux['name'];
        // $value['reachabilityStatus'] = $entity_aux['reachabilityStatus'];
        // $value['macAddress'] = $entity_aux['macAddress'];
        // $value['ipAddress'] = $entity_aux['ipAddress'];
        // $value['upTime'] = $entity_aux['upTime'];
        // $value['locationHierarchy'] = $entity_aux['locationHierarchy'];
        // $value['last_updated'] = $entity_aux['last_updated'];
        //
        // ## insert into Mysql
        // $columns ="id, name, reachability_status, mac_address, ip_address, uptime, location_hierarchy, last_updated";
        // $values = "'".implode("', '",$value)."'";
        // $sql = "REPLACE INTO prime_access_points ($columns) VALUES ($values)";
        // var_dump($sql);
        // $mar_conn->query($sql);

    }
    $mon_conn->executeBulkWrite("svnms.tb_prime_ap", $bulk);
    $first+=$max;
    echo $first;
}
?>
