<?php
include '../config.inc.php';
# MongoDB indexing : db.prime_devices.ensureIndex({"id": 1})
#set variables
$milliseconds = round(microtime(true) * 1000);
$last_updated = $milliseconds;
$url = 'https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v3/data/Devices.json';

$tablePrime = '`svnms`.`prime`';

$first = 0;
$max = 1000;
$mar_conn = new mysqli($DB['HOST'], $DB['USER'], $DB['PASS'], $DB['NAME']);
$mon_conn = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");

#Infinity curl

while ($first>-1) {
    $date1 = time();
    $bulk = new MongoDB\Driver\BulkWrite([]);
    # curl the API
    $listDevicesUrl= "$url?.full=true&.firstResult=$first&.maxResults=$max";
    $listDevices = exec("curl -s -k '$listDevicesUrl'");

    #get return to array
    $listDevicesArray = json_decode($listDevices,true);
    $entities = $listDevicesArray['queryResponse']['entity'];
    $statusChecking = $listDevicesArray['queryResponse']['@count'];

    #break if no output
    if (!$entities) break;

    foreach ($entities as $entity) {

        $document = $entity_aux = array();
        $entity_aux['@id'] = $entity['devicesDTO']['@id'];
        $entity_aux['collectionDetail'] = $entity['devicesDTO']['collectionDetail'];
        $entity_aux['collectionStatus'] = $entity['devicesDTO']['collectionStatus'];
        $entity_aux['collectionTime'] = $entity['devicesDTO']['collectionTime'];
        $entity_aux['creationTime'] = $entity['devicesDTO']['creationTime'];
        $entity_aux['deviceId'] = $entity['devicesDTO']['deviceId'];
        $entity_aux['deviceName'] = $entity['devicesDTO']['deviceName'];
        $entity_aux['deviceType'] = $entity['devicesDTO']['deviceType'];
        $entity_aux['ipAddress'] = str_replace(' ', '', $entity['devicesDTO']['ipAddress']);
        $entity_aux['managementStatus'] = $entity['devicesDTO']['managementStatus'];
        $entity_aux['manufacturerPartNrs'] = $entity['devicesDTO']['manufacturerPartNrs'];
        if ($entity_aux['manufacturerPartNrs']) {
    		$entity_aux['manufacturerPartNrs'] = implode(",",$entity_aux['manufacturerPartNrs']);
	}
	$entity_aux['productFamily'] = $entity['devicesDTO']['productFamily'];
        $entity_aux['reachability'] = trim($entity['devicesDTO']['reachability']);
        $entity_aux['softwareType'] = $entity['devicesDTO']['softwareType'];
        $entity_aux['softwareVersion'] = $entity['devicesDTO']['softwareVersion'];
        $entity_aux['last_updated']= $milliseconds;

        $document['where'] = array('@id' => $entity_aux['@id']);
        $document['data'] = array('$set' => $entity_aux);
        $document['options'] = array('multi' => false, 'upsert' => true);
        $bulk->update(['@id' => $entity_aux['@id']], ['$set' => $entity_aux], ['multi' => false, 'upsert' => true]);


        $value['id'] = $entity_aux['@id'];
        $value['deviceId'] = $entity_aux['deviceId'];
        $value['deviceName'] = $entity_aux['deviceName'];
        $value['deviceType'] = $entity_aux['deviceType'];
        $value['ipAddress'] = $entity_aux['ipAddress'];
        $value['managementStatus'] = $entity_aux['managementStatus'];
        $value['productFamily'] = $entity_aux['productFamily'];
        $value['reachability'] = $entity_aux['reachability'];
        $value['last_updated'] = $entity_aux['last_updated'];

        ## insert into Mysql
	      $columns ="id, deviceId, deviceName, deviceType, ipAddress, managementStatus, productFamily, reachability, last_updated";
        $values = "'".implode("', '",$value)."'";
        $sql = "REPLACE INTO prime_devices ($columns) VALUES ($values)";
     	// var_dump($sql);
	$mar_conn->query($sql);
    }
      ## insert into MongoDB
    $mon_conn->executeBulkWrite("svnms.prime_devices", $bulk);
    $first+=$max;
    echo $first;
}
?>
