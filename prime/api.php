<?php

include_once(dirname(__DIR__) . '/config.inc.php');
date_default_timezone_set("Asia/Kuala_Lumpur");

array_shift($argv);

foreach ($argv as $value) {
	$getApiList = api_list('prime');
	if (isset($getApiList[$value])) {
		$data = requestPrimeApi($value,'GET',$getApiList[$value]);
		// $insertDataMongo = updateMongo($value,$data);
		// $insertDataMaria = updateMaria($value,$data);
	} elseif ($argv[0] == 'list') {
		echo "List of api:\n";
		print_r(array_keys($getApiList));
	} else {
		echo "No argument of '$value'\n";
	}
}

#Request Api
function requestPrimeApi($name,$method,$url){

	$first = 0;
	$max = 1000;
	$last = $first;

	do {


		$newUrl = $url.".firstResult=".$first."&.maxResults=".$max;

		$cmd = "curl -k \"$newUrl\"";

		$output = shell_exec("$cmd");
		$outputArray = json_decode($output,true);

		if (isset($outputArray['queryResponse']['entity'])) {
			$entities = $outputArray['queryResponse']['entity'];
			$last = $outputArray['queryResponse']['@last'];
			$updateDB= sortData($name,$entities);
			$checkingEntity = 'valid';
		} else {
			$checkingEntity = 'invalid';
		}

		$first+=$max;

	} while ($checkingEntity == 'valid');


	echo "\nTotal: $last \n";
	
	return $output;
}

function sortData($name,$data){

	foreach ($data as $entity) {
		if ($name == 'alarms' || $name == 'alarms_filtered') {
			$entity['id'] = $entity['alarmsDTO']['@id'];
			$updateMaria = primeAlarms($name,$entity);
		}else if ($name == 'devices') {
			$entity['id'] = $entity['devicesDTO']['@id'];
			$updateMaria = primeDevices($name,$entity);
		}else if ($name == 'access_points') {
			
			// $updateMaria = primeAccessPoints($name,$data);
		}
		// print_r($entity);
		// $upsertMongo = upsertMongo($name,$entity);
	}

}

#Upsert services into mongo
function upsertMongo($name,$data){

	try {

	    $conn = new MongoDB\Driver\Manager("mongodb://192.168.220.51:27017");
	      # Insert data

		$id = $data['id'];

		$bulk = new MongoDB\Driver\BulkWrite([]);
		$bulk->update(
			['id' => $id],
			['$set' => $data],
			['upsert' => true]
		);
	    $result = $conn->executeBulkWrite("svnms.prime_".$name, $bulk);


	} catch (MongoDB\Driver\Exception\Exception $e) {

		$filename = basename(__FILE__);

		echo "The $filename script has experienced an error.\n";
		echo "It failed with the following exception:\n";

		echo "Exception:", $e->getMessage(), "\n";
		echo "In file:", $e->getFile(), "\n";
		echo "On line:", $e->getLine(), "\n";
	}

}



#Update services into maria
function primeAlarms($name,$data){

	$tableName = '`svnms`.`prime_'.$name.'`';
	$column = "`id`,`acknowledgementStatus`,`alarmFoundAt`,`category_ordinal`,`category_value`,`condition_ordinal`,`condition_value`,`deviceName`,`message`,`nttyaddrss7_address`,`severity`,`source`,`timeStamp`,`wirelessSpecificAlarmId`";
	$conn = database_connect($GLOBALS['DB']);
	$date = date('Y-m-d H:i:s');

	$entity['id'] = $data['alarmsDTO']['@id'];
    $entity['acknowledgementStatus'] = $data['alarmsDTO']['acknowledgementStatus'];
    $entity['alarmFoundAt'] = $data['alarmsDTO']['alarmFoundAt'];
    $entity['alarmID'] = $data['alarmsDTO']['alarmId'];
    $entity['category_ordinal'] = $data['alarmsDTO']['category']['ordinal'];
    $entity['category_value'] = $data['alarmsDTO']['category']['value'];
    $entity['condition_ordinal'] = $data['alarmsDTO']['condition']['ordinal'];
    $entity['condition_value'] = $data['alarmsDTO']['condition']['value'];
    $entity['deviceName'] = $data['alarmsDTO']['deviceName'];
    $entity['lastUpdatedAt'] = $data['alarmsDTO']['lastUpdatedAt'];
    $entity['message'] = $data['alarmsDTO']['message'];
    $entity['nttyaddrss7_address'] = str_replace(" ","",$data['alarmsDTO']['nttyaddrss7_address']);
    $entity['severity'] = trim($data['alarmsDTO']['severity']);
    $entity['source'] = $data['alarmsDTO']['source'];
    $entity['timestamp_prime'] = $data['alarmsDTO']['timeStamp'];
    $entity['wirelessSpecificAlarmId'] = $data['alarmsDTO']['wirelessSpecificAlarmId'];

    $entityVal = "\"".implode('", "',$entity)."\"";
    $sql = "REPLACE INTO $tableName ($column) VALUES ($entityVal)";

    // if ($conn->query($sql) === FALSE) {
    //     echo "Error creating/updating record: " . $conn->error."\n ID: ".$entity['id']."\n";
    // }

	// print_r($sql);


}

function primeDevices($name,$data){

	$tableName = '`svnms`.`prime_'.$name.'`';
	$column = "`id`,`collectionDetail`,`collectionStatus`,`collectionTime`,`creationTime`,`deviceId`,`deviceName`,`deviceType`,`ipAddress`,`managementStatus`,`manufacturerPartNrs`,`productFamily`,`reachability`,`softwareType`,`softwareVersion`";
	$conn = database_connect($GLOBALS['DB']);
	$date = date('Y-m-d H:i:s');

	$entity['id'] = $data['devicesDTO']['@id'];
    $entity['collectionDetail'] = $data['devicesDTO']['collectionDetail'];
    $entity['collectionStatus'] = $data['devicesDTO']['collectionStatus'];
    $entity['collectionTime'] = $data['devicesDTO']['collectionTime'];
    $entity['creationTime'] = $data['devicesDTO']['creationTime'];
    $entity['deviceId'] = $data['devicesDTO']['deviceId'];
    $entity['deviceName'] = $data['devicesDTO']['deviceName'];
    $entity['deviceType'] = $data['devicesDTO']['deviceType'];
    $entity['ipAddress'] = $data['devicesDTO']['ipAddress'];
    $entity['managementStatus'] = $data['devicesDTO']['managementStatus'];
    $entity['manufacturerPartNrs'] = $data['devicesDTO']['manufacturerPartNrs']['manufacturerPartNr'];
    if ($entity['manufacturerPartNrs']) {
    	$entity['manufacturerPartNrs'] = implode(",",$entity['manufacturerPartNrs']);
    }
    $entity['productFamily'] = $data['devicesDTO']['productFamily'];
    $entity['reachability'] = $data['devicesDTO']['reachability'];
    $entity['softwareType'] = $data['devicesDTO']['softwareType'];
    $entity['softwareVersion'] = $data['devicesDTO']['softwareVersion'];

    $entityVal = "'".implode("', '",$entity)."'";
    $sql = "REPLACE INTO $tableName ($column) VALUES ($entityVal)";
    echo "\n".$entity['id']."\n";

    // if ($conn->query($sql) === FALSE) {
    //     echo "Error creating/updating record: " . $conn->error."\n ID: ".$entity['id']."\n";
    // }

    // print_r($sql);

}

function primeAccessPoints($name,$data){

	$conn = database_connect($GLOBALS['DB']);
	$date = date('Y-m-d H:i:s');


}



