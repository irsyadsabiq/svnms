<?php

include '../config.inc.php';
date_default_timezone_set("Asia/Kuala_Lumpur");

$conn = new mysqli($DB['HOST'], $DB['USER'], $DB['PASS'], $DB['NAME']);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

#set variables
$time = time();
$url = 'https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v3/data/Alarms.json';

$tablePrime = '`svnms`.`prime`';

$first = 0;
$max = 1000;
$tablePrimeAlarms = '`svnms`.`prime_alarm`';
$columnPrimeAlarms = "`id`,`acknowledgementStatus`,`alarmFoundAt`,`category_ordinal`,`category_value`,`condition_ordinal`,`condition_value`,`deviceName`,`message`,`nttyaddrss7_address`,`severity`,`source`,`timeStamp`,`wirelessSpecificAlarmId`";

#Infinity curl
while ($first>-1) {
    # curl the API
    $listAlarmsUrl= "$url?.full=true&.firstResult=$first&.maxResults=$max";
    $listAlarms = exec("curl -k '$listAlarmsUrl'");

    #get return to array
    $listAlarmsArray = json_decode($listAlarms,true);
    $statusChecking = $listAlarmsArray['queryResponse']['@count'];
    $entities = $listAlarmsArray['queryResponse']['entity'];
    $entities = str_replace("'","",$entities);


    #checking status
    if (!$statusChecking) {
        $statusVal = '0';
    }else {
        $statusVal = '1';
    }
    $sql = "UPDATE $tablePrime SET `status`='".$statusVal."', `last_updated`='".$time."' WHERE `name`='Alarms'";

    if ($conn->query($sql) === FALSE) {
        echo "Error updating status: " . $conn->error."\n";
    }

    #break if no output
    if (!$entities) break;

    # Delete/Insert into DB
    foreach ($entities as $entity) {

        $alarmsArray['id'] = $entity['alarmsDTO']['@id'];
        $alarmsArray['acknowledgementStatus'] = $entity['alarmsDTO']['acknowledgementStatus'];
        if ($alarmsArray['acknowledgementStatus'] == '') {
            $alarmsArray['acknowledgementStatus'] = 0;
        }
        $alarmsArray['alarmFoundAt'] = $entity['alarmsDTO']['alarmFoundAt'];
        $alarmsArray['category_ordinal'] = $entity['alarmsDTO']['category']['ordinal'];
        $alarmsArray['category_value'] = $entity['alarmsDTO']['category']['value'];
        $alarmsArray['condition_ordinal'] = $entity['alarmsDTO']['condition']['ordinal'];
        $alarmsArray['condition_value'] = $entity['alarmsDTO']['condition']['value'];
        $alarmsArray['deviceName'] = $entity['alarmsDTO']['deviceName'];
        $message = $entity['alarmsDTO']['message'];
        $alarmsArray['message'] = str_replace("'","",$message);
        $alarmsArray['nttyaddrss7_address'] = $entity['alarmsDTO']['nttyaddrss7_address'];
        $alarmsArray['severity'] = $entity['alarmsDTO']['severity'];
        $alarmsArray['source'] = $entity['alarmsDTO']['source'];
        $alarmsArray['timeStamp'] = $entity['alarmsDTO']['timeStamp'];
        $alarmsArray['wirelessSpecificAlarmId'] = $entity['alarmsDTO']['wirelessSpecificAlarmId'];

        $entityVal = "'".implode("', '",$alarmsArray)."'";
        $sql = "REPLACE INTO $tablePrimeAlarms ($columnPrimeAlarms) VALUES ($entityVal)";

        if ($conn->query($sql) === FALSE) {
            echo "Error creating/updating record: " . $conn->error."\n ID: ".$alarmsArray['id']."\n";
        }
    }

    $first+=$max;
}

$conn->close();
echo "end";

