##prime API##
curl -k "https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v3/data/Devices"
curl -k "https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v3/data/Devices?.full=true"
curl -k "https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v3/data/Devices?.full=true&.sort=ipAddress"
curl -k "https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v1/data/Devices/15"
curl -k "https://172.21.68.4/webacs/api/v1/data/Devices/15.json"
curl -k "https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v1/data/ApiHealthRecords/"
curl -k "https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v1/data/ApiHealthRecords?.full=true"
curl -k "https://tmucs:JustDoIt2019@172.21.68.4/webacs/api/v3/op/statisticsService/system/health?range=72"

##Aruba airwave##
##authentication API##
curl -k -c /tmp/cjar -d "credential_0=Dune" -d "credential_1=Dune@123" -d "destination=/ap_list.xml" -d "login=Log In" http://10.54.1.91:80/LOGIN
curl -k -b /tmp/cjar http://10.54.1.91:80/ap_detail.xml

curl -k -b /tmp/cjar --output ap_detail.xml https://10.54.1.91/ap_detail.xml


##CMX API##
##Client
curl -k "https://duneadmin:Dune1234@172.21.68.12/api/location/v2/clients/count" ## number of client,
curl -k "https://duneadmin:Dune1234@172.21.68.12/api/location/v2/clients" ## All client details

## client details include

curl -k "https://duneadmin:Dune1234@172.21.68.12/api/location/v3/clients/count"
## get all APS172.21.68.12/api/config/v1/maps/count
curl -k "https://duneadmin:Dune1234@172.21.68.12/api/config/v1/aps" #AP details
curl -k "https://duneadmin:Dune1234@172.21.68.12/api/location/v1/rogueaps/count" # no result
curl -k "https://duneadmin:Dune1234@172.21.68.12/api/config/v1/maps/count"
curl -k "https://duneadmin:Dune1234@172.21.68.12/api/config/v1/maps" ## AP details
curl -k "https://duneadmin:Dune1234@172.21.68.12/api/analytics/v1/connectedDetected" ## connectedDetected

curl -k "https://duneadmin:Dune1234@172.21.68.12/api/location/v1/attributes"
curl -k "https://duneadmin:Dune1234@172.21.68.12/api/location/v1/tags" #empty
